-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2016 at 10:43 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `miniphonebook`
--

-- --------------------------------------------------------

--
-- Table structure for table `phonebook`
--

CREATE TABLE IF NOT EXISTS `phonebook` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `group` varchar(255) NOT NULL,
  `profile_picture` varchar(255) NOT NULL,
  `added_by` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phonebook`
--

INSERT INTO `phonebook` (`id`, `name`, `mobile`, `group`, `profile_picture`, `added_by`) VALUES
(47, 'OwesQuruni Shuvo', '01929112222', 'Other', '/SecurePhonebook/resource/uploads/shubho.jpg', 'admin@admin.com'),
(48, 'Robin SIsh', '01721111444', 'Friends', '/SecurePhonebook/resource/uploads/alooz.jpg', 'towfiqpiash@gmail.com'),
(49, 'Towfiqur Piash', '01722486102', 'Friends', '/SecurePhonebook/resource/uploads/piash.jpg', 'tahmina@yahoo.com'),
(50, 'Tahmina Akter', '0188212211', 'Work', '/SecurePhonebook/resource/uploads/sloth.JPG', 'hasin@webmail.com'),
(54, 'Bill Gates', '666777', 'Friends', '/SecurePhonebook/resource/uploads/default.jpg', 'hasin@webmail.com'),
(55, 'Homo Sapiece', '989898', 'Work', '/SecurePhonebook/resource/uploads/uuu.jpg', 'towfiqpiash@gmail.com'),
(57, 'Rifat Alam', '01267761721', 'Family', '/SecurePhonebook/resource/uploads/default.jpg', 'admin@admin.com'),
(59, 'Ahmed Al-Hussain', '3234234234', 'Friends', '/SecurePhonebook/resource/uploads/hossain.jpg', 'admin@admin.com'),
(60, 'Abdul Kader', '01766232222', 'Friends', '/SecurePhonebook/resource/uploads/default.jpg', 'tahmina@yahoo.com');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`) VALUES
(13, 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e'),
(14, 'towfiqpiash@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(15, 'tahmina@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e'),
(16, 'hasin@webmail.com', 'e10adc3949ba59abbe56e057f20f883e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `phonebook`
--
ALTER TABLE `phonebook`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `phonebook`
--
ALTER TABLE `phonebook`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
