<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#crudNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="../../index.php">
                <span class="glyphicon glyphicon-book"></span> PhoneBook
            </a>
        </div>
        <div class="collapse navbar-collapse" id="crudNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?= isset($_SESSION['userid'])?$_SESSION['userid']:'' ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="../authentication/logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                    </ul>
                  </li>
            </ul>
        </div>
    </div>
</nav>