<?php

include_once '../startup.php';

use App\User\Auth;
use App\Utility\Direction;
use App\Contact\Phonebook;
use App\Utility\Message;

$objAuth = new Auth();
$status = $objAuth->is_loggedin();

if($status == false){
    return Direction::redirect("../../index.php"); 
} else{
    $objContact = new Phonebook();
    $data = $objContact->index();
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>SecurePhonebook :: Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php require_once('../Layout/common_style.php'); ?>
        <link rel="stylesheet" href="../../resource/css/jquery.dataTables.min.css">

        <style>
            @media (max-width: 360px) {
                #addBtn_row{
                    text-align: center;
                }

                #addBtn{
                    float: none !important;
                }
            }
            .dataTables_wrapper .dataTables_paginate {
                float: right;
                padding-top: 0.755em;
                text-align: right;
            }

            .dataTables_wrapper .dataTables_paginate .paginate_button {
                background-color: #fff;
                border: 1px solid #ddd;
                color: #337ab7 !important;
                float: left;
                line-height: 1.42857;
                margin-left: -1px;
                padding: 6px 12px;
                position: relative;
                text-decoration: none;
            }

            .dataTables_wrapper .dataTables_paginate .paginate_button:hover{
                background: none !important;
                background-color: #eee !important;
                color: #337ab7 !important;
                border-color: #ddd !important;
            }

            .dataTables_wrapper .dataTables_paginate .paginate_button.current, 
            .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
                background: none !important;
                background-color: #337ab7 !important;
                color: #fff !important;
                cursor: default;
            }

            .dataTables_wrapper .dataTables_paginate .paginate_button.disabled, 
            .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:hover, 
            .dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
                background: none !important;
                border-color: #ddd;
                cursor: default;
            }

        </style>
    </head>

    <body>

        <?php require_once('../Layout/navbar.php'); ?>

        <div class="container">
            <h1 class="text-center"><span class="glyphicon glyphicon-phone-alt"></span> SecurePhonebook<span class="glyphicon glyphicon-phone-alt"></span></h1>
            <hr/>
            <?php 
                if(isset($_SESSION['message'])){
                    echo Message::message();
                }
            ?>
            <div class="row text-center" id="addBtn_row">
                <a href="create.php" id="addBtn" class="btn btn-success"><span class="glyphicon glyphicon-user"></span> Add New Contact</a>
            </div>
            <div>&nbsp;</div>
            <div class="row text-center">Download list as: 
                <a href="download-pdf.php" title="Download as PDF"><img src="../../resource/img/pdf.ico" alt="pdf" style="height: 32px"></a> | 
                <a href="download-xls.php" title="Download as XLS"><img src="../../resource/img/xls.ico" alt="xls" style="height: 32px"></a>
            </div>
            <div>&nbsp;</div>
            <div class="row col-md-8 col-md-offset-2">
                <table id="list" class="table table-responsive table-striped table-bordered dt-responsive" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($data as $record){
                        ?>
                        <tr>
                            <td>
                                <a href="show.php?id=<?= $record->id; ?>" style="text-decoration: none; color: inherit; ">
                                    <img src="<?= $record->profile_picture; ?>" class="img-rounded" alt="user" height="50">
                                </a>
                            </td>
                            <td>
                                <a href="show.php?id=<?= $record->id; ?>" style="text-decoration: none; color: inherit; ">
                                    <h4 style="margin-top: 0"><?= $record->name; ?></h4>
                                    <small><?= $record->mobile; ?></small>
                                </a>
                            </td>
                            <td>
                                <a href="tel:<?= $record->mobile; ?>"><button class="btn btn-default btn-lg"><span class="glyphicon glyphicon-phone-alt"></span> Dial</button></a>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <?php require_once('../Layout/footer.php'); ?>
        <?php require_once('../Layout/common_script.php'); ?>
        <script src="../../resource/js/jquery.dataTables.min.js"></script>
        
        <script>
            $(document).ready(function () {
                $('#list').DataTable();
            });
            
            $('.alert').fadeOut(4000);
        </script>

    </body>
</html>