<?php

include_once '../startup.php';

use App\Contact\Phonebook;

$objContact = new Phonebook();
$contacts = $objContact->index();

$trs = "";
?>

<?php

$slno = 0;
foreach ($contacts as $contact):
    $slno++;
    $trs .="<tr>";
    $trs .="<td>" . $slno . "</td>";
    $trs .="<td>" . $contact->name . "</td>";
    $trs .="<td>" . $contact->mobile . "</td>";
    $trs .="<td>" . $contact->group . "</td>";
    $trs .="</tr>";
endforeach;
?>

<?php

$html = <<<FLAG
<!DOCTYPE html>
<html>
    <head>
        <title>Contact Sheet</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .delete{
                border:1px solid red;
            }
        </style>
    </head>
    
    <body>
        <h1>Contact Sheet</h1>
     
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>Group</th>
                </tr>
            </thead>
            <tbody>
                echo $trs;
            </tbody>
        </table>
     </body>
</html>
FLAG;
?>
<?php

require_once $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . "SecurePhonebook" . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . 'mpdf' . DIRECTORY_SEPARATOR . 'mpdf' . DIRECTORY_SEPARATOR . 'mpdf.php';

$mpdf = new mPDF('c');
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;